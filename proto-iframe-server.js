
var restify = require('restify');
var config = require('./config.json');
var moment = require('moment');
var util = require('util');
var request = require('request-promise');
var chance = require('chance').Chance();
var mailer = require('nodemailer').createTransport(config.nodemailer);
var iFrameServer = restify.createServer(config.server);

//restify middleware
iFrameServer.pre(function corsHandler(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8889');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, POST ');
  res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
  res.setHeader('Access-Control-Max-Age', '1000');
  return next();
});
iFrameServer.use(restify.queryParser());  //parse body before passing request onto handlers
iFrameServer.use(restify.bodyParser());  //parse body before passing request onto handlers

iFrameServer.get('/test',restify.serveStatic({
          directory: __dirname,
          file: '/test.html'
}));
console.log(__dirname);
iFrameServer.get('/osp_form',restify.serveStatic({
          directory: __dirname+'/osp-form',
          file: '/osp-form.html'
}));
iFrameServer.post('/osp_form',(req,res)=>{
  console.log('reveived a new post',req.params)
  var testMailOptions = {
    from: "vbforeman@gmail.com",
    // to: "vbforeman@gmail.com",
    to: "brandon@onesourceprocess.com",
    subject: "testing mailer",
    "text": JSON.stringify(req.params,null,2)
  };
  mailer.sendMail(testMailOptions,(err,info)=>{
      if (err) {
            return console.log(err);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
      res.send(req.params)
  });
});
iFrameServer.listen(config.server.port,function(){
  console.log('%s listening at %s',iFrameServer.name,iFrameServer.url);
});
